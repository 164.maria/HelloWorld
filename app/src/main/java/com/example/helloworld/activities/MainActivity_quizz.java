package com.example.myapplication777;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.helloworld.R;

public class MainActivity_quizz extends AppCompatActivity implements View.OnClickListener {

    int currentQuestionIndex = 0;
    int score = 0;
    int totalQuestions = QuestionAnswer.question.length;

    TextView totalQuestionsTextView;
    TextView questionTextView;

    Button ansA;
    Button ansB;
    Button ansC;

    Button submitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        totalQuestionsTextView = findViewById(R.id.totalQuestions);
        questionTextView = findViewById(R.id.question);

        ansA = findViewById(R.id.ans_A);
        ansB = findViewById(R.id.ans_B);
        ansC = findViewById(R.id.ans_C);

        submitBtn = findViewById(R.id.submit_btn);


        totalQuestionsTextView.setText("Total Questions: " + totalQuestions);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadNewQuestion();

    }

        ansA = (Button)findViewById(R.id.ansA);
        ansA.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            currentQuestionIndex ++;
            if(currentQuestionIndex < totalQuestions) {
                loadNewQuestion();
            } else {
                totalQuestionsTextView.setText("Your score is:" + score);
                score += 3;

            }


        }
    });

        ansB = (Button)findViewById(R.id.ansB);
        ansB.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            currentQuestionIndex ++;
            if(currentQuestionIndex < totalQuestions) {
                loadNewQuestion();
            } else {
                totalQuestionsTextView.setText("Your score is:" + score);
                score += 2;

            }


        }
    });

        ansC = (Button)findViewById(R.id.ansC);
        ansB.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
                currentQuestionIndex ++;
                (currentQuestionIndex < totalQuestions) {
                loadNewQuestion();
            } else {
                totalQuestionsTextView.setText("Your score is:" + score);
                score += 1;

            }


        }
    });


    void loadNewQuestion(){
        questionTextView.setText(QuestionAnswer.question[currentQuestionIndex]);
        ansA.setText(QuestionAnswer.choice[currentQuestionIndex][0]);
        ansB.setText(QuestionAnswer.choice[currentQuestionIndex][0]);
        ansC.setText(QuestionAnswer.choice[currentQuestionIndex][0]);

        if(currentQuestionIndex==totalQuestions)
        {
            finishQuiz();
            return;
        }
    }

    void finishQuiz(){
        String results = "";


        if (score > totalQuestions*0.60){
            results = "You did well";
        }
        else {
            results = "you can always try again and do better";
        }

        new AlertDialog.Builder(this)
                .setTitle(results)
                .setMessage("Your score is: " + score + "out of" + totalQuestions)
                .setPositiveButton("Restart", (dialogueInterfaces, i) -> restartQuiz());
    }

    void restartQuiz()
    {
        score = 0;
        currentQuestionIndex = 0;
        loadNewQuestion();
    }

}

