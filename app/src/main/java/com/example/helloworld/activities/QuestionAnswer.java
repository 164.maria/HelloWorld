package com.example.my_application1;

public class QuestionAnswer {
    public static String question[] = {
            ///Realist
            "Would you like to work in a bank?",
            "Would you like to administrate the data of a company?",
            "Would you like to work with a computer or a printer?",
            "Would you like to book flights, hotels for other people? ",
            "Would you like to be an accountant?",
            "Would you like to take care of a budget of a company?",
            "Would you like to keep track of stuff that is consumed in a company?",
            "Would you like to work with numbers?",
            "Would you like to study at a company and elaborate a system good for financial problems?",
            "Would you like to supervise the employee of a company?",
            "Would you like to take up a math course for financial problems?",
            "Would you like to verify the documents of banks in order to find mistakes?",
            "Would you like to use the computer to take up financial data?",
            "Would you like to start an accountancy?",
            "Would you like to put information in a calculator?",
            "Would you like to take your record for some works?",
            "Would you like to work with cash?",
            "Would you like to be a receptionist in a hotel?",
            "Would you like to administrative employee?",
            "Would you like to with numbers on a computer?",
    };

    public static String question2[] = {
            ///ARTISTIC :
            "Would you like to sing on a stage?",
            "Would you like to be an artist?",
            "Would you like to write short stories?",
            "Would you like to make cartoons?",
            "Would you like to teach music in schools?",
            "Would you like to write a novel?",
            "Would you like to organize/direct plays?",
            "Would you like to be a jazz singer?",
            "Would you like to read articles about music and art?",
            "Would you like to be the designer of magazine or TV ads?",
            "Would you like to study the works of great musicians?",
            "Would you like to write a play?",
            "Would you like to compose music or make musical arrangements?",
            "Would you like to conduct a symphony orchestra?",
            "Would you like to write magazine articles?",
            "Would you like to paint landscapes?",
            "Would you like to write TV scripts?",
            "Would you like to do set design for plays?",
            "Would you like to arrange the background music for the film?",
            "Would you like to do book reviews like a literary critic?"
};
    public static String question3[] = {
            ///SOCIAL :
            "Would you like to talk with different people about the problems concerning the community?",
            "Would you like to help physically disabled people prepare for a job?",
            "Would you like to help the helpless  (disabled, elderly, children with problems)?",
            "Would you like to be a teacher?",
            "Would you like to study sociology, i.e. how people live together (social groups)?",
            "Would you like to advise on poor people's legislation?",
            "Would you like to take care of sick people?",
            "Would you like to help people who have come out of prison to find a job?",
            "Would you like to learn how to qualify adults for a job?",
            "Would you like to help people choose a career?",
            "Would you like to supervise people who have broken the law?",
            "Would you like to participate in charity fundraisers?",
            "Would you like to plan the activity of others?",
            "Would you like to give first aid?",
            "Would you like to work as a family counselor?",
            "Would you like to help children with mental disorders?",
            "Would you like to learn how to help people in pain?",
            "Would you like to study psychology?",
            "Would you like to coordinate the sports program on a sports field?",
            "Would you like to conduct group discussions for delinquent children?"
};
    public static String question4[] ={
            ///ENTERPRISING :
            "Would you like to run an administrative department?",
            "Would you like to buy merchandise for a big store?",
            "Would you like to be a sales manager?",
            "Would you like to register workers who have workplace grievances?",
            "Would you like to earn money from trading or the stock market?",
            "Would you like to run a big restaurant?",
            "Would you like to deal with administrative politics?",
            "Would you like to be a bank vice president?",
            "Would you like to be the jurisconsult of a company?",
            "Would you like to take a leadership position?",
            "Would you like to be a real estate agent?",
            "Would you like to work to convince the government to take a certain action?",
            "Would you like to promote the development of a new population supply market?",
            "Would you like to do business, trade?",
            "Would you like to be the legislator who mediates disputes between unions and employers?",
            "Would you like to be a judge?",
            "Would you like to recruit and hire people for a large company?",
            "Would you like to travel around the country to sell the company's products?",
            "Would you like to organize and coordinate business?",
            "Would you like to be a production manager?"
    };

    public static String question5[] =
            {
                    ///Investigativ
                    "Would you like to do scientific experiments?",
                    "Would you like to use a microscope for studying cells and bacterias?",
                    "Would you like to read books and scientific magazines?",
                    "Would you like to do research work in a biology laboratory?",
                    "Would you like to study on the Moon, the Sun, the planets and the stars?",
                    "Would you like to study heart diseases?",
                    "Would you like to use mathemathics to solve a technical and scientific problem?",
                    "Would you like to be a surgeon?",
                    "Would you like to be a marine biologist?",
                    "Would you like to examine the effects of polluted air on the environment?",
                    "Would you like to invent a new type of technical equipment?",
                    "Would you like to do scientific research on the use of solar energy?",
                    "Would you like to make an artificial heart?",
                    "Would you like to be a laboratory nurse?",
                    "Would you like to take a biology course at school or at the university?",
                    "Would you like to research and to find a cure for cancer?",
                    "Would you like to guide specific studies on plant diseases?",
                    "Would you like to be a doctor who likes to help and prevent diseases?",
                    "Would you like to do scientific studies about nature?",
                    "Would you like to help scientific researchers in their experimental laboratories?"
            };




    public static String choice[][] = {
            {"yes", "maybe", "no"}
    };

}
