package com.example.helloworld.listeners;

public interface UserListener {
    void onUserClicked(User user);
}
