package com.example.helloworld.listeners;

public interface ConversionListener {
    void onConversionClicked(User user);
}
